import { Component, OnInit } from '@angular/core';
import { HomeComponent } from '../home/home.component';
import { CricketBoardComponent } from '../cricket-board/cricket-board.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-toss',
  templateUrl: './toss.component.html',
  styleUrls: ['./toss.component.css']
})
export class TossComponent implements OnInit {

  public static country1;
  public static country2;

  selectedcountryList: any = [{ id: localStorage.getItem("country1id"), name: localStorage.getItem("country1name"), src: localStorage.getItem("country1src"), checkbox: localStorage.getItem("country1chk") },
  { id: localStorage.getItem("country2id"), name: localStorage.getItem("country2name"), src: localStorage.getItem("country2src"), checkbox: localStorage.getItem("country2chk") }]

  selectedItems: any[] = [];
  counter: number = 0;
  disabled: boolean = true;
  country1: number = 0;
  country2: number = 0;


  constructor(private router: Router) { }

  ngOnInit() {

  }

  checkedState(event, checkBox: number) {
    if (event.target.checked == true) {
      CricketBoardComponent.whoWillBowl = checkBox

    }
  }

  play(){
    this.router.navigate(["cricket-board"])
  }
}
