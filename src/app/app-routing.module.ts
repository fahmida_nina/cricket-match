import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MatchComponent } from './match/match.component';
import { TossComponent } from './toss/toss.component';
import { CricketBoardComponent } from './cricket-board/cricket-board.component';


const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  // { path: '', component: MatchComponent, pathMatch: 'full' },
  { path: 'toss', component: TossComponent, pathMatch: 'full' },
  { path: 'cricket-board', component: CricketBoardComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
