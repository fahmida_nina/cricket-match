import { Component, OnInit } from '@angular/core';
import { $ } from 'protractor';
import { Router } from '@angular/router';
import { TossComponent } from '../toss/toss.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  countryList: any = [{ id: 1, name: 'Bangladesh', src: './../../assets/img/Flags/BD.png', checkbox: 'myCheckbox1' },
  { id: 2, name: 'India', src: './../../assets/img/Flags/IND.png', checkbox: 'myCheckbox2' },
  { id: 3, name: 'Pakistan', src: './../../assets/img/Flags/Pk.png', checkbox: 'myCheckbox3' },
  { id: 4, name: 'New Zealand', src: './../../assets/img/Flags/NZ.png', checkbox: 'myCheckbox4' },
  { id: 5, name: 'England', src: './../../assets/img/Flags/Eng.png', checkbox: 'myCheckbox5' },
  { id: 5, name: 'SriLanka', src: './../../assets/img/Flags/SriLanka.png', checkbox: 'myCheckbox6' }]


  selectedItems: any[] = [];
  counter: number = 0;
  disabled: boolean = true;
  country1: number = 0;
  country2: number = 0;
  constructor(private router: Router) {

  }

  ngOnInit() {
  }


  checkedState(event, checkBox: number) {
    if (event.target.checked == true) {
      this.counter = this.counter + 1;

      if (this.country1 == 0) {
        this.country1 = checkBox
      } else {
        this.country2 = checkBox
      }

    } else if (event.target.checked == false) {
      this.counter = this.counter - 1;

    }
    if (this.counter > 2) {
      this.disabled = true
    } else {
      this.disabled = false
    }

    console.log(this.country1, this.country2, "here")

    return this.counter;

  }

  play(value) {
    if (this.country1 != 0 && this.country2 != 0) {
      TossComponent.country1 = this.country1;
      TossComponent.country2 = this.country2;
    }

    localStorage.setItem("country1id", this.countryList.find(x => x.id == this.country1).id)
    localStorage.setItem("country1name", this.countryList.find(x => x.id == this.country1).name)
    localStorage.setItem("country1src", this.countryList.find(x => x.id == this.country1).src)
    localStorage.setItem("country1chk", this.countryList.find(x => x.id == this.country1).checkbox)
    localStorage.setItem("country2id", this.countryList.find(x => x.id == this.country2).id)
    localStorage.setItem("country2name", this.countryList.find(x => x.id == this.country2).name)
    localStorage.setItem("country2src", this.countryList.find(x => x.id == this.country2).src)
    localStorage.setItem("country2chk", this.countryList.find(x => x.id == this.country2).checkbox)
    this.router.navigate(["toss"])
  }

}
