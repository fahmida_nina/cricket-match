import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { MatchComponent } from './match/match.component';
import { TossComponent } from './toss/toss.component';
import { CricketBoardComponent } from './cricket-board/cricket-board.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MatchComponent,
    TossComponent,
    CricketBoardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
