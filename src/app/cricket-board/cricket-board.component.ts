import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cricket-board',
  templateUrl: './cricket-board.component.html',
  styleUrls: ['./cricket-board.component.css']
})

export class CricketBoardComponent implements OnInit {

  public static country1;
  public static country2;
  public static whoWillBowl;

  min: number = 1;
  max: number = 6;

  selectedcountryList: any = [{ id: localStorage.getItem("country1id"), name: localStorage.getItem("country1name"), src: localStorage.getItem("country1src"), checkbox: localStorage.getItem("country1chk") },
  { id: localStorage.getItem("country2id"), name: localStorage.getItem("country2name"), src: localStorage.getItem("country2src"), checkbox: localStorage.getItem("country2chk") }]

  listBowls: Array<any> = []
  country1Name: string;
  country2Name: string;
  _whowillbowl: any;

  constructor() { }


  ngOnInit() {
    this.country1Name = localStorage.getItem("country1name");
    this.country2Name = localStorage.getItem("country2name");
    this._whowillbowl = CricketBoardComponent.whoWillBowl;
  }

  randomNumber() {

    let random = Math.floor(Math.random() * (this.max - this.min) + this.min);
    return random;
  }

  bowlcounter: number = 0;
  run: number = 0;
  bowl() {
    if (Math.abs((this.bowlcounter % 1) - .6) < .0000001)
      this.bowlcounter += .5;
    else
      this.bowlcounter += .1;

    this.run = this.randomNumber();
     this.listBowls.push(this.bowlcounter, this.run)

  }
}
